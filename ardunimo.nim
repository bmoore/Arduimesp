# Some reasonable set of imports for a sketch
import Arduino, common, pins_arduino


# Convenience templates, nifty
template setup(code: untyped): untyped =
  proc setup*() {.exportc.} =
    code

template loop(code: untyped): untyped =
  proc loop*() {.exportc.} =
    code

# Initialize Nim since Arduino will not do it
proc NimMain() {.importc.}
NimMain()
